# oVirt Engine Metrics

#### 介绍
oVirt.metrics角色可以设置oVirt Metrics存储，在引擎和虚拟机管理程序上部署rsyslog/fluentd和收集以及管理服务。

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
