# oVirt Engine Metrics

#### Description
The oVirt.metrics role enables you to set up oVirt Metrics store, deploy rsyslog/fluentd and collectd on the engine and hypervisors and manage the services.

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request
