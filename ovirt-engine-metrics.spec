# Copyright (C) 2017 Red Hat, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

%global         package_version 1.3.6.4

%global metrics_data %{_datadir}/ovirt-engine-metrics
%global metrics_roles_data %{_datadir}/ansible/roles/oVirt.metrics
%global metrics_etc %{_sysconfdir}/ovirt-engine-metrics

%global make_common_opts \\\
	PREFIX=%{_prefix} \\\
	SYSCONF_DIR=%{_sysconfdir} \\\
	DATAROOT_DIR=%{_datadir} \\\
	LOCALSTATE_DIR=%{_localstatedir} \\\
	BUILD_VALIDATION=0

Summary:        oVirt Engine Metrics
Name:           ovirt-engine-metrics
Version:        1.3.6.4
Release:        3
Source0:        http://resources.ovirt.org/pub/src/ovirt-engine-metrics/ovirt-engine-metrics-1.3.6.4.tar.gz
License:        ASL 2.0
Group:          Virtualization/Management
BuildArch:      noarch
Url:            http://www.ovirt.org

Requires:	ansible >= 2.8.3
Requires:	ovirt-ansible-image-template >= 1.1.11
Requires:	ovirt-ansible-vm-infra >= 1.1.19
Requires:	python3-jmespath

# In oVirt 4.1.0, content of this package was in a different one, which was
# built from the engine sources.
Obsoletes:	ovirt-engine-hosts-ansible-inventory
Provides:	ovirt-engine-hosts-ansible-inventory

%description
ovirt-engine-metrics allows configuring an oVirt installation to send metrics
data to a remote metrics store.

%prep
%setup -c -q

%build
make %{make_common_opts}

%install
make %{make_common_opts} install DESTDIR="%{buildroot}"

%clean
rm -rf %{buildroot}

%files
%license LICENSE
%{metrics_data}
%{metrics_roles_data}
%{metrics_etc}
%{metrics_etc}/config.yml.d
%doc README.md

%changelog
* Mon Apr  1 2024 yanjianqing <yanjianqing@kylinos.cn> - 1.3.6.4-3
- Modify the changelog

* Tue Sep 14 2021 huanghaitao <huanghaitao8@huawei.com> - 1.3.6.4-2
- Fixes requires python2-jmespath to python3-jmespath

* Tue Mar  3 2020 changjie.fu <changjie.fu@cs2c.com.cn> - 1.3.6.4-1
- Package Initialization
